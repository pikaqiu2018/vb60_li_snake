# VB6.0贪吃蛇snake源代码
Visual Basic 6.0 创作出的纯代码贪吃蛇。它同时支持贪吃蛇的穿墙功能。

可以学习游戏循环fps循环的使用。
还有me.line的用法。

好吧，不再继续说了。简单说就是一个贪吃蛇的源码，学习这个源码可以很方便地抑制到安卓/ios 上。
因为我觉得VB6很接近伪代码。

上图：
![游戏界面](https://gitee.com/uploads/images/2018/0328/143611_b9ba87a8_121085.jpeg "捕获.JPG")

下面是游戏的部分代码：

![代码1](https://gitee.com/uploads/images/2018/0328/143537_1191b6fd_121085.jpeg "34.JPG")

![代码](https://gitee.com/uploads/images/2018/0328/143637_8d54a999_121085.jpeg "22.JPG")

